
<a name="1.8.3"></a>
## [1.8.3](/compare/1.8.2...1.8.3) (2021-11-25)

### Add

* add mail limit on expire to config

### Fix

* fix variable name


<a name="1.8.2"></a>
## [1.8.2](/compare/1.8.1...1.8.2) (2021-11-16)

### Set

* set cron task as daily

### Update

* update cron time and delay


<a name="1.8.1"></a>
## [1.8.1](/compare/1.8.0...1.8.1) (2021-11-16)

### Disable

* disable auto expiration

### Send

* send expiration notif to admin


<a name="1.8.0"></a>
## [1.8.0](/compare/1.7.9...1.8.0) (2021-11-15)

### Remove

* remove ldap sync file from this repo


<a name="1.7.9"></a>
## [1.7.9](/compare/1.7.8...1.7.9) (2021-10-21)

### Enable

* enable cron task config + create systemd timer


<a name="1.7.8"></a>
## [1.7.8](/compare/1.7.6...1.7.8) (2021-10-15)

### Allow

* allow more local test with molecule
* allow to use molecule for local test


<a name="1.7.6"></a>
## [1.7.6](/compare/1.7.5...1.7.6) (2021-10-12)

### Change

* change default ui show

### Fix

* fix for ansible lint
* fix nodejs verion

### Update

* update nodejs on ubuntu focal too
* update node version (clean modification

### Reverts

* fix nodejs verion


<a name="1.7.5"></a>
## [1.7.5](/compare/1.7.4...1.7.5) (2021-07-07)

### Add

* add config for registration option


<a name="1.7.4"></a>
## [1.7.4](/compare/1.7.3...1.7.4) (2021-06-16)

### Add

* add a var for ssl cert template
* add .cache in git ignore

### Manage

* manage ssl cert and key path for nginc


<a name="1.7.3"></a>
## [1.7.3](/compare/1.7.2...1.7.3) (2021-06-07)

### Add

* add ldap-utils install as it is needed in real world


<a name="1.7.2"></a>
## [1.7.2](/compare/1.7.1...1.7.2) (2021-05-31)

### Add

* add an option to add env variable in my node env file


<a name="1.7.1"></a>
## [1.7.1](/compare/1.7.0...1.7.1) (2021-05-28)

### Disable

* disable homedir check for user

### Enable

* enable debug on ldap sync


<a name="1.7.0"></a>
## [1.7.0](/compare/1.6.5...1.7.0) (2021-05-27)

### Add

* add var for focal (mongo 4.4)
* add ubuntu + ubuntu18

### Try

* try to upgrade mongo to 4.2


<a name="1.6.5"></a>
## [1.6.5](/compare/1.6.4...1.6.5) (2021-05-20)

### Add

* add some space for ansible lint

### Use

* use full path on remote for base64 image generation
* use jinja variable for base64 image
* use delegate_to instead of local_action
* use slurp to lookup file


<a name="1.6.4"></a>
## [1.6.4](/compare/1.6.2...1.6.4) (2021-05-20)

### Add

* add .cache in gitignore

### Allow

* allow logo modification + usage in email

### Fix

* fix base64 image gen
* fix ansible syntax
* fix yalm syntax


<a name="1.6.2"></a>
## [1.6.2](/compare/1.6.3...1.6.2) (2021-04-21)


<a name="1.6.3"></a>
## [1.6.3](/compare/1.6.1...1.6.3) (2021-04-21)

### Add

* add theme management
* add source map on ng build

### Use

* use remote src to copy theme


<a name="1.6.1"></a>
## [1.6.1](/compare/1.6.0...1.6.1) (2021-03-24)

### Add

* add prevent_reuse option in config
* add libffi

### Fix

* fix variable to avoid python False transformation
* fix gitlab-ci lib install

### Install

* install molecule-docker pip package

### Try

* try to make lint work
* try with new docker molecule image
* try with default image for gitlab build
* try to use /cert for docker in docker


<a name="1.6.0"></a>
## [1.6.0](/compare/1.5.9...1.6.0) (2020-12-02)

### Disable

* disable log debug

### Fix

* fix project import (bad check if exist)

### Rewrite

* Rewrite Ldap Sync

### Update

* update console log level

### Merge Requests

* Merge branch 'work_on_sync' into 'master'


<a name="1.5.9"></a>
## [1.5.9](/compare/1.5.8...1.5.9) (2020-11-06)

### Clean

* clean node_modules dir and try to use less space for this


<a name="1.5.8"></a>
## [1.5.8](/compare/1.5.7...1.5.8) (2020-11-04)

### Fix

* fix import service path

### Update

* update for new cron directory tree


<a name="1.5.7"></a>
## [1.5.7](/compare/1.5.6...1.5.7) (2020-10-27)

### Add

* add a smart comment
* add check if object exist in mongo before import them

### Fix

* fix duplicate code

### Remove

* remove await
* remove useless dump
* remove useless debug
* remove useless comment


<a name="1.5.6"></a>
## [1.5.6](/compare/1.5.5...1.5.6) (2020-10-02)

### Add

* add default expire for project
* add ansible-lint config file

### Set

* set some default to false


<a name="1.5.5"></a>
## [1.5.5](/compare/1.5.4...1.5.5) (2020-07-07)

### Add

* add project config

### Remove

* remove tmp as tmpfs on molecule


<a name="1.5.4"></a>
## [1.5.4](/compare/1.5.3...1.5.4) (2020-07-03)

### Add

* add X-Frame-Options header


<a name="1.5.3"></a>
## [1.5.3](/compare/1.5.2...1.5.3) (2020-06-25)

### Use

* use ubuntu18 for molecule test
* use default template as default


<a name="1.5.2"></a>
## [1.5.2](/compare/1.5.1...1.5.2) (2020-06-16)

### Add

* add the ci option to npm

### Update

* update angular version


<a name="1.5.1"></a>
## [1.5.1](/compare/1.5.0...1.5.1) (2020-06-12)

### Reverts

* do not filter on ldap posix group


<a name="1.5.0"></a>
## [1.5.0](/compare/1.4.3...1.5.0) (2020-06-12)

### Do

* do not filter on ldap posix group


<a name="1.4.3"></a>
## [1.4.3](/compare/1.4.2...1.4.3) (2020-06-04)

### Fix

* fix nginx http redirect


<a name="1.4.2"></a>
## [1.4.2](/compare/1.4.1...1.4.2) (2020-06-02)

### Fix

* fix ansible syntax for nginx

### Remove

* Remove old test

### Use

* use last nginx role version


<a name="1.4.1"></a>
## [1.4.1](/compare/1.3.1...1.4.1) (2020-05-20)

### Add

* add a step to add mongo Compatibility Version
* add a step to remove old mongo db package

### Fix

* fix (again) ansible syntax
* fix ansible syntax

### Force

* force autoremove for old mongo package

### Remove

* remove more package from old mongo install
* remove useless variable

### Switch

* Switch back to mongo 3.6

### Upgrade

* upgrade to last mongo version

### Use

* use mongodb official repo + some clean

### Reverts

* Switch back to mongo 3.6


<a name="1.3.1"></a>
## [1.3.1](/compare/1.2.3...1.3.1) (2020-05-19)

### Clean

* clean white space

### Improve

* improve cron script shell

### Run

* run script every 30s

### Try

* Try to enable redis database

### Update

* update CHANGELOG


<a name="1.2.3"></a>
## [1.2.3](/compare/1.2.2...1.2.3) (2020-05-11)

### Try

* try to run systemd script every 2 inute (120 sec)

### Try

* Try to improve systemd Path whatcher for scripts run


<a name="1.2.2"></a>
## [1.2.2](/compare/1.2.1...1.2.2) (2020-04-29)

### Do

* do not run my config gen on idempotence test

### Fix

* fix molecule verify + disable all idempotence

### Use

* use last molecule version (update config + remove goss)


<a name="1.2.1"></a>
## [1.2.1](/compare/1.2.0...1.2.1) (2020-04-28)

### Remove

* remove quota from project expiration mail

### Rename

* rename deletion mail to user_deletion


<a name="1.2.0"></a>
## [1.2.0](/compare/1.1.0...1.2.0) (2020-04-24)

### Add

* add some default mail template
* add email for request to add user to project
* add expiration mail
* add a mail template when the user is added to project
* add registration email message in config

### Fix

* fix nginx ansible role version
* fix again yaml syntax
* fix yaml syntax + spelling

### Remove

* remove useless --


<a name="1.1.0"></a>
## [1.1.0](/compare/1.0.5...1.1.0) (2020-03-13)

### Add

* add active status to synched user
* add an import service too
* add the $set command to mongo to try to update well
* add a sync specific script

### Clean

* clean email footer template + add some red

### Release

* Release 1.0.5


<a name="1.0.5"></a>
## [1.0.5](/compare/1.0.4...1.0.5) (2020-01-10)

### Add

* add a variable for account duration
* add NODE_PATH in env
* add sync service + enable script timer
* add sync service + enable script timer
* add scripts timer for shell exec
* add a var to disable user group
* add a var to disable user group
* add a call to ldap import on scripts services

### Disable

* disable cron
* disable sync cron
* disable wait loop in gomngr + enable old script dir

### Fix

* fix node path
* fix nginx role version

### Mirror

* mirror favicon

### Update

* update timer
* update timer

### Reverts

* add a call to ldap import on scripts services
* fix nginx role version


<a name="1.0.4"></a>
## [1.0.4](/compare/1.0.3...1.0.4) (2019-12-11)

### Add

* add default minuid and mingid


<a name="1.0.3"></a>
## [1.0.3](/compare/1.0.2...1.0.3) (2019-12-09)

### Release

* Release 1.0.3


<a name="1.0.2"></a>
## [1.0.2](/compare/1.0.1...1.0.2) (2019-12-09)

### Add

* add ask project in mail template

### Improve

* improve mail sent template
* improve activation mail about password and project


<a name="1.0.1"></a>
## [1.0.1](/compare/1.0.0...1.0.1) (2019-12-05)

### Add

* add variable in template ...
* add default home web variable


<a name="1.0.0"></a>
## [1.0.0](/compare/0.9.13...1.0.0) (2019-12-04)

### Add

* add delettion message
* add link in footer for text message too


<a name="0.9.13"></a>
## [0.9.13](/compare/0.9.12...0.9.13) (2019-12-02)

### Add

* add a variable for ng build option

### Remove

* remove default --prod for ng build option + add a s to this var


<a name="0.9.12"></a>
## [0.9.12](/compare/0.9.11...0.9.12) (2019-11-15)

### Move

* move mail config in specific default file

### Remove

* remove p from message html footer + add a link for my url

### Use

* use main directory to load several default


<a name="0.9.11"></a>
## [0.9.11](/compare/0.9.10...0.9.11) (2019-10-31)

### Add

* add auto_add_group variable + use default group for admin
* add another config variable
* add some config variable
* add port in molecule for local test
* add default username max len to 256

### Change

* change default registration group

### Fix

* fix true for var

### Remove

* remove version for  undergreen.mongodb

### Try

* try to wait for lock free on script exec
* try to run gomngr more often + add some sleep

### Use

* use false as default for all unneeded ui part

### Reverts

* add another config variable


<a name="0.9.10"></a>
## [0.9.10](/compare/0.9.9...0.9.10) (2019-10-28)

### Fix

* fix task name

### Use

* use a hash list for enable_ui variable (for config)


<a name="0.9.9"></a>
## [0.9.9](/compare/0.9.8...0.9.9) (2019-10-25)

### Add

* add bansec config value

### Disable

* disable centos test as it s...

### Increase

* increase ansible timeout in molecule

### Move

* Move old scripts file after 10 day

### Readd

* readd mongo role version because ansible galaxy is unable to manage it ...4

### Remove

* remove version for molecule test
* remove version from meta

### Update

* update .gitignore
* update .gitignore

### Use

* use default branch for default test
* use official git repo as default for installation


<a name="0.9.8"></a>
## [0.9.8](/compare/0.9.7...0.9.8) (2019-10-14)

### Disable

* disable debian test, as mongodb repo is not ready for debian buster

### Update

* update CHANGELOG


<a name="0.9.7"></a>
## [0.9.7](/compare/0.9.6...0.9.7) (2019-10-10)

### Update

* update ansible mongo version


<a name="0.9.6"></a>
## [0.9.6](/compare/0.9.5...0.9.6) (2019-10-10)

### Disable

* Disable expire cron


<a name="0.9.5"></a>
## [0.9.5](/compare/0.9.4...0.9.5) (2019-10-10)

### Decrease

* decrease sleep time for scripts execution

### Fix

* fix log directory creation path

### Remove

* remove forced npm_config_prefix

### Try

* try to add cron service for some node script


<a name="0.9.4"></a>
## [0.9.4](/compare/0.9.3...0.9.4) (2019-10-04)

### Release

* Release 0.9.4

### Switch

* switch to wip-ifb for git branch on molecule test


<a name="0.9.3"></a>
## [0.9.3](/compare/0.9.2...0.9.3) (2019-10-04)

### Install

* Install Epel only for CentOs

### Release

* Release 0.9.3

### Update

* Update putty dep version and source link


<a name="0.9.2"></a>
## [0.9.2](/compare/0.9.1...0.9.2) (2019-10-02)

### Add

* add a changelog

### Fix

* fix path for scripts log

### Update

* update url in CHANGELOG


<a name="0.9.1"></a>
## [0.9.1](/compare/0.8.4...0.9.1) (2019-10-01)

### Add

* add logorate
* add a secret in config for auth in my

### Fix

* fix group perm for rsyslog
* fix log file name wildcard
* fix check on logrotate directory

### Set

* set rsyslog user to root on debian

### Try

* try to create a specific dir for syslog file

### Use

* use my_orga as name for my


<a name="0.8.4"></a>
## [0.8.4](/compare/0.8.3...0.8.4) (2019-09-23)

### Add

* add a link to my in the footer of email


<a name="0.8.3"></a>
## [0.8.3](/compare/0.8.2...0.8.3) (2019-09-19)

### Add

* add variable for mailing sympa list


<a name="0.8.2"></a>
## [0.8.2](/compare/0.8.1...0.8.2) (2019-09-17)

### Add

* add variable for enable_ui

### Try

* Try to add a config option for the terms of use link

### Use

* use quote to prevente f to be transformed in F (for false)


<a name="0.8.1"></a>
## [0.8.1](/compare/0.7.7...0.8.1) (2019-09-05)

### Add

* add term of use file for ifb
* add some adds to my orga

### Allow

* allow disable of user_extra_dir and plugin

### Clean

* clean term of use (remove some space + svg ref)

### Disable

* disable stapling
* disable qa style check for list of ssl ciphers

### Try

* try to add some security to ssl nginx config


<a name="0.7.7"></a>
## [0.7.7](/compare/0.7.6...0.7.7) (2019-09-04)

### Fix

* fix cert path


<a name="0.7.6"></a>
## [0.7.6](/compare/0.7.5...0.7.6) (2019-09-04)

### Add

* add my_local_url variable


<a name="0.7.5"></a>
## [0.7.5](/compare/0.7.4...0.7.5) (2019-09-04)

### Add

* add a variable to allow setting server name to be different from inventory name


<a name="0.7.4"></a>
## [0.7.4](/compare/0.7.3...0.7.4) (2019-09-03)

### Add

* add template for ssl certifate


<a name="0.7.3"></a>
## [0.7.3](/compare/0.7.2...0.7.3) (2019-08-28)

### Add

* add a variable for admin list


<a name="0.7.2"></a>
## [0.7.2](/compare/0.7.1...0.7.2) (2019-08-28)

### Add

* Add Basic ssl and cert generation


<a name="0.7.1"></a>
## [0.7.1](/compare/0.6.3...0.7.1) (2019-08-27)

### Add

* add nginx reverse proxy


<a name="0.6.3"></a>
## [0.6.3](/compare/0.6.2...0.6.3) (2019-08-27)

### Add

* add a variable for the organisation name (in mail template)

### Remove

* remove sleep in script executor


<a name="0.6.2"></a>
## [0.6.2](/compare/0.6.1...0.6.2) (2019-08-22)

### Add

* add a time on runner script
* add a sleep in the runner script
* add exit code in log filename too
* add default variable for my_force_rebuild_angular

### Move

* move the sleep in my script runner to be done on each update

### Remove

* remove time as it dont work
* remove recurse option + logo check as it is always changed

### Set

* set test extra dir in user home

### Try

* try to have better log on my scripts execution
* try to manage angular rebuild


<a name="0.6.1"></a>
## [0.6.1](/compare/0.5.6...0.6.1) (2019-08-19)

### Add

* add env load on cron script


<a name="0.5.6"></a>
## [0.5.6](/compare/0.5.5...0.5.6) (2019-08-19)

### Trigger

* Trigger Gitlab Runner

### Use

* use ifb as default templates


<a name="0.5.5"></a>
## [0.5.5](/compare/0.5.4...0.5.5) (2019-08-14)

### Disable

* disable production mode on npm install

### Update

* update angular version to install


<a name="0.5.4"></a>
## [0.5.4](/compare/0.5.3...0.5.4) (2019-08-14)

### Use

* use production mode for npm module install


<a name="0.5.3"></a>
## [0.5.3](/compare/0.5.2...0.5.3) (2019-08-13)

### Update

* update minimum ansible version


<a name="0.5.2"></a>
## [0.5.2](/compare/0.5.1...0.5.2) (2019-08-13)

### Add

* add template and mailer in config
* add ugly delete on tp reservation

### Use

* use same version for test on all host


<a name="0.5.1"></a>
## [0.5.1](/compare/0.4.2...0.5.1) (2019-08-09)

### Add

* add putty as dep for ssh keygen in my user interface
* add a variable for access log path in app env
* add a chmod on cron script
* add exit code in cron result filename
* add a template to generate cron file
* add a delay to script service before start (to give time to my to finish writing scripts files
* add a copy of the ifb logo
* add a restart of my scripts too
* add some test for extra dir creation feature

### Clear

* clear my log on ugly delete

### Disable

* disable bower install, use ci for npm install and --prod for angular build

### Fix

* fix ng build command
* fix variable name

### Install

* install distinct version on dev server to be able to compare
* install my version with template on molecule test

### Try

* try to use a specific node version for different os (as centos is too old)

### Update

* update git url for putty install + version increase
* update ansible putty install version

### Use

* use my test template version for test on dev server


<a name="0.4.2"></a>
## [0.4.2](/compare/0.4.1...0.4.2) (2019-07-30)

### Add

* add some variable for support and accounts email
* add smtp host in local test

### Update

* update local test

### Use

* use admin email as default value for other email in conf
* use my email for my test :)


<a name="0.4.1"></a>
## [0.4.1](/compare/0.4.0...0.4.1) (2019-07-26)

### Add

* add var for my dependencies

### Disable

* disable idempotence as included role for mongo is not idempotent

### Fix

* fix npm install dir to be as standard as possible

### Install

* install node global package while installing node as npm global does not work very well...

### Rename

* rename include var task

### Try

* try to clean dependency installation process


<a name="0.4.0"></a>
## [0.4.0](/compare/0.3.7...0.4.0) (2019-07-24)

### Update

* update default admin group
* update manual test

### Use

* use myadmin as default admin login


<a name="0.3.7"></a>
## [0.3.7](/compare/0.3.6...0.3.7) (2019-07-24)

### Remove

* remove var my_openldap_admin_dn as it should always be my_openldap_dn


<a name="0.3.6"></a>
## [0.3.6](/compare/0.3.5...0.3.6) (2019-07-24)

### Add

* add explicit state for npm install


<a name="0.3.5"></a>
## [0.3.5](/compare/0.3.4...0.3.5) (2019-07-23)

### Add

* add one command for each node package install (as it look like it's not working well with list)


<a name="0.3.4"></a>
## [0.3.4](/compare/0.3.3...0.3.4) (2019-07-23)

### Add

* add a clear of fact cache to help debug

### Update

* update config indentation


<a name="0.3.3"></a>
## [0.3.3](/compare/0.3.2...0.3.3) (2019-07-23)

### Install

* install more node tools globally


<a name="0.3.2"></a>
## [0.3.2](/compare/0.3.1...0.3.2) (2019-07-22)

### Add

* add node mailer conf
* add docker:dind
* add variable for default user group
* add more ugly wildcard on molecule test preparation

### Downgrade

* downgrade node and angular version to fit actual version used by my test

### Fix

* fix json syntax in config

### Remove

* remove event and projet too before molecule install
* remove DEBUG var in .env
* remove prod flag for angular build

### Update

* update test on vm

### Use

* use localhost as default smtp
* use gitlab.com url for molecule requirement
* use nodemailer for notif
* use default value for ldap ou people and group
* use node 10 for molecule test
* use wip dev branch for my git clone
* use lts version for nodejs


<a name="0.3.1"></a>
## [0.3.1](/compare/0.3.0...0.3.1) (2019-06-03)

### Add

* add a variable for group in user home path managment
* add a variable for users home dir
* add dotenv in usefull tools

### Set

* set use group in user home path default to true

### Try

* try to add debug in env file
* try to fix false value for user group in home path

### Update

* update syslog to have a separate file for each my job


<a name="0.3.0"></a>
## [0.3.0](/compare/0.2.3...0.3.0) (2019-05-29)

### Add

* add molecule role dir to git ignore

### Fix

* fix mongodb requirement version as it is needed with new ansible-galaxy

### Update

* update openldap server version


<a name="0.2.3"></a>
## [0.2.3](/compare/0.2.2...0.2.3) (2019-05-28)

### Add

* add variable for ldap ou

### Create

* create readme dir

### Fix

* fix admin group

### Kiss

* kiss test on vm

### Set

* set molecule option to only have one -v
* set full test on vm + variable on check

### Update

* update openldap requierement to have password hash
* update openldap version for molecule and vm test

### Use

* use lang for readme dir


<a name="0.2.2"></a>
## [0.2.2](/compare/0.2.1...0.2.2) (2019-05-27)

### Add

* Add some vvv to ansible (launched by molecule)

### Add

* add a restart before test on vm
* add a touch to log file for ugly test
* add a playbook to show ldap and mongo data
* add a service to run generated shell script
* add the missing D answer...
* add a script to delete mongo users
* add var for admin user + restart service on some more change
* add a systemd reload on service configuration change
* add log to /var/log/my.log + rename systemd service
* add log to /var/log/my.log + rename systemd service
* add scripts dir creation to allow my to run various shell command
* add some debug info
* add git update to test on vm
* add empty dockerfile for molecule
* add empty dockerfile for molecule
* add dotenv + use var for git url

### Bugfix

* bugfix ansible extra var for vm tests
* bugfix variable assignation + remove useless and commented ldap load

### Check

* check is rsyslog exist before trying to copy conf file

### Clean

* clean test script for vm
* clean code + use handler to start my

### Disable

* disable pymongo install for molecule test
* disable mysql installation in molecule as it take too much time

### Enable

* enable privileged for molecule test as it look like debian and ubuntu need it to run slapd with systemd

### Fix

* fix copy/past to quick
* fix target for systemd run
* fix systemd template name

### Force

* force git in test on vm

### Log

* log every my in my.log

### Move

* move from post_tasks to tasks as it look like noqa dont work for those
* move main dependencies to prepare part of molecule
* move specific os var file to molecule dir

### Re

* re enable force on role download as if not there is no update

### Remove

* remove useless todo in env template
* remove useless debug msg
* remove useless goss test for molecule
* remove useless dependencies from requierement
* remove condition in config template
* remove space from config file
* remove useless task file
* remove useless ldap init file

### Rename

* rename prepare playbook
* rename app to my in systemd service
* rename gitlab pipeline step

### Set

* set full test on vm test
* set role path to only have my in tmp_dir
* set a random instance for molecule docker image (to allow parrallele run)

### Try

* try to improve my service management
* try to bugfix ugly hack
* try to be less ugly in the role path for molecule
* try to flush handler in molecule playbook after role execution
* try to run all distro test in parallele
* try to add more color in gitlab
* try again with null value on config template

### Ugly

* ugly hack to avoid download of all dependencies on each run

### Uglyer

* uglyer delete for vm test

### Update

* update openldap requierement to have people and group
* update test on vm
* update gitignore to stop showing *retry
* update ldap requirement
* update description for my service
* update config template to all null without quote on servers host
* update molecule verify step to check if my is running
* update Readme
* update Readme

### Use

* use a different syslog id for my script
* use generic command to create role directory for gitlabci
* use custom docker image for molecule test (with systemd)
* use tmpdir to run molecule
* use custom docker image for gitlab run + log with time on molecule run
* use system to run my app
* use new var from openldap server role
* use master branch for openldap

### Merge Requests

* Merge branch 'remove_all_useless_dep' into 'master'


<a name="0.2.1"></a>
## [0.2.1](/compare/0.2.0...0.2.1) (2019-05-13)

### Rename

* rename openldap to openldap-server


<a name="0.2.0"></a>
## [0.2.0](/compare/0.1.0...0.2.0) (2019-05-10)

### Adapt

* adapt gitlab ci to use molecule

### Add

* add a test on slapd running in molecule
* add openldap git repo as requirement
* add default prepopulate for ldap
* add forever start to run app
* add jinga template for my conf + move dep to included task + try to full install app
* add a smart comment (or not)
* add gcc in molecule docker file, as it may or may not be needed by some tools
* add mongo + rabbitmq + redis + mailhog install
* add test if mongod is running for molecule
* add mongodb install + try to run depencies only when needed
* add gnupg to molecule dockerfile as it is needed for several ansible repo add
* add a test (not in molecule) to check if remote host answer to attended port
* add molecule for test
* add flag for services installation + use "my" as reference name for variable (as it is the name of the application)
* add mysql (mariadb) installation

### Bugfix

* bugfix ldap domain name
* bugfix ansible syntax

### Clean

* clean requirements and dependency
* clean CentOs var
* clean dependency in test requirement
* clean dependency + try to control mariadb installation

### Disable

* disable check for mariadb
* disable mariadb install
* disable idempotence test in molecule (as openldap installation is not idempotence ready)
* disable openldap install
* disable mailhog install as it is useless and it don't work well
* disable repel repo (on centos)
* disable docker and pip installation
* disable centos for test

### Disable

* Disable centos build on test

### Do

* do not disable ldap over ssl by default

### Enable

* enable standard install
* enable mariadb repository for any platform test

### Fix

* fix ansible openldap version
* fix space comment
* fix for yaml lint
* fix for yaml lint
* fix syntax for ansible lint
* fix space for yaml lint

### Include

* include file for mariadb installation + move some vars

### Lint

* lint yaml

### Move

* move ldap prepolulate data + disable the load of this data

### Remove

* remove useless test (as we now use molecule)
* remove useless molecule file INSTALL.rst
* remove mailhog dep as me disable it
* remove docker compose call
* remove useless test
* remove docker clean (should use another role for this)

### Rename

* rename task which launch my genouest

### Rename

* Rename role to use the name of the installed app

### Run

* run all the role

### Set

* set env for node config file

### Small

* small test script for our dev platform

### Stop

* stop molecule on first error on any installation target

### Switch

* switch ldap role
* switch rabbitmq install role + start installation process for openldap

### Try

* try to be idempotent
* try to not have space in conf file value from jinga
* try again to use systemd in molecule container + try again on openldap installtion + debug
* try to use systemd on any molecule docker platform
* try to be more and more idempotent
* try to be idempotent (again)
* try to be indepotent
* try to install a more recent version of ansible before run test on centos
* try to install a more recent version of ansible before run test on ubuntu
* try to use geerlingguy for package npm

### Update

* update molecule step sequence order
* update centos version

### Use

* use variable for git clone
* use ansible hostname instead of localhost
* use the same yamllint conf as galaxy for molecule test
* use ubuntu dev vm for tests

### Merge Requests

* Merge branch 'the_same_without_docker' into 'master'
* Merge branch 'master' into 'the_same_without_docker'


<a name="0.1.0"></a>
## [0.1.0](/compare/0.0.4...0.1.0) (2019-04-19)

### Add

* add user param in test inventory

### Check

* check if we are on redhat in test requirements

### Clean

* Clean main task file + move test to test.yml

### Get

* get git lab ci pipeline def from ansible-webmin

### Move

* move test to test.yml

### Remove

* remove useless remote_user param


<a name="0.0.4"></a>
## [0.0.4](/compare/0.0.3...0.0.4) (2019-04-17)

### Add

* add hostname for all host

### Check

* check if uri is availlable after install

### Disable

* disable ansible service and use docker compose

### Try

* try to ignore image from registry


<a name="0.0.3"></a>
## [0.0.3](/compare/0.0.2...0.0.3) (2019-04-16)

### Add

* Add LICENSE
* Add LICENSE

### Delete

* Delete LICENSE

### Merge

* Merge branch 'master' of gitlab.cluster.france-bioinformatique.fr:taskforce/ansible-genouestaccountmanager

### Try

* try to manage osallou/my

### Update

* Update licence for galaxy meta info

### Use

* use ansible to replace docker compose
* use $(pwd) instead of relative path


<a name="0.0.2"></a>
## [0.0.2](/compare/0.0.1...0.0.2) (2019-04-12)

### Add

* Add --diff option for tests
* Add Basic readme and no information about the licence

### Fix

* fix yaml syntax

### Install

* install redhat repo only if we are on redhat ^_^

### Make

* make it work on redhat too


<a name="0.0.1"></a>
## 0.0.1 (2019-04-11)

### Add

* add ugly hack to have acces to repo as roles for test
* add tag to run step too

### Move

* move prepare step to before_script

### Remove

* Remove tags + try to use docker image for gitlab run

### Rename

* rename role name in test

### Report

* Report dev from main ansible repo

### Test

* test tag to run somewhere else

### Try

* try to run command on local (without ssh)

### Use

* use localhost to run test

